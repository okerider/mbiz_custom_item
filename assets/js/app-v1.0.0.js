/*--------------------*/
// define canvas
var frontShirt = new canvasCustomItem('canvas-frontShirt'); // create canvas instance for front shirt
var backShirt = new canvasCustomItem('canvas-backShirt'); // create canvas instace for back shirt

var activeShirt = 'frontShirt';

/*--------------------*/
// functions
// trigger change background on click
$('.change-background a img').click(function(){
  let bg = $(this).attr('src');
  let targetObject = $(this).closest('.bg-options').attr('data-object');
  if(targetObject == 'frontShirt'){
    frontShirt.canvasBackground(bg);
  }else{
    backShirt.canvasBackground(bg);
  }
});

// change shirt
$('.change-shirt a').click(function(){
  let targetObject = $(this).attr('data-object');
  // change active shirt
  activeShirt = targetObject;
  // show and hide option
  $('.change-background .bg-options').addClass('hidden');
  $('.change-background .bg-options.' + targetObject).removeClass('hidden');
  // show and hide canvas
  $('.canvas-wrapper').addClass('hidden');
  $('.canvas-wrapper#canvas-'+targetObject).removeClass('hidden');
});

// upload custom image
$('.upload-image').click(function(evt){
  evt.preventDefault();
  $('#custom-upload-image').click();
});
$('input#custom-upload-image').change(function(){
  let FR = new FileReader();
  FR.onload = function(e){
    if(activeShirt == 'frontShirt'){
      frontShirt.addItem( 'image', e.target.result, 'upload' );
    }else{
      backShirt.addItem( 'image', e.target.result, 'upload' );
    }
    $('input#custom-upload-image').val('');
  }
  FR.readAsDataURL( this.files[0] );
});

// add clipart
$('.btn-add-clipart').click(function(){
  let selectedClipart = $('#add-clipart form input:checked').val();
  if(activeShirt == 'frontShirt'){
    frontShirt.addItem( 'image', selectedClipart , 'clipart' );
  }else{
    backShirt.addItem( 'image', selectedClipart , 'clipart' );
  }
  $('#add-clipart').modal('hide');
  $('#add-clipart form input:checked').prop('checked', false);
});

// add text
var updatePreview = function( properties ){
  let text = $('#add-text-value').val();
  $('#add-text-preview').text( text );
  $('#add-text-preview').attr('data-value', text);
  if(properties){
    $('#add-text-preview').css(properties);
  }
}
$('#add-text-value').on('keyup',function(){
  if( $(this).val().length == 0 ){
    updatePreview( 'masukan text' );
  }else{
    $('#add-text-preview').attr('data-value', $(this).val());
    updatePreview();
  }
});
$('#add-text-color').change(function(){
  updatePreview( { color : '#' + $(this).val() } );
});
$('#add-text-family').change(function(){
  updatePreview( { fontFamily : $(this).val() } );
});
$('.btn-add-text').click(function(){
  let textValue = $('#add-text-preview').attr('data-value');
  let properties = {
    fontFamily : $('#add-text-family').val(),
    color : ('#' + $('#add-text-color').val())
  };
  if( $('#add-text').attr('data-editing') == 'true' ){
    let textId = $('#add-text').attr('data-id');
    if(activeShirt == 'frontShirt'){
      frontShirt.editText(textId, textValue, properties);
    }else{
      backShirt.editText(textId, textValue, properties);
    }
  }else{
    if(activeShirt == 'frontShirt'){
      frontShirt.addItem( 'text', textValue, properties );
    }else{
      backShirt.addItem( 'text', textValue, properties );
    }
  }
  $('#add-text').attr('data-editing','false').modal('hide');
});
$('.btn-add-text').click(function(){
  $('#add-text').attr('data-editing', 'false');
});

// delete currently selected object
$(document).keyup(function(e) {
  if(e.keyCode == 46){
    if(activeShirt == 'frontShirt'){
      frontShirt.deleteSelected();
    }else{
      backShirt.deleteSelected();
    }
  }
});
$('.delete-selected').click(function(evt){
  evt.preventDefault();
  if(activeShirt == 'frontShirt'){
    frontShirt.deleteSelected();
  }else{
    backShirt.deleteSelected();
  }
});

// export images
$('#export-shirt').click(function(){
  let frontShirtResult = frontShirt.toImages();
  let backShirtResult = backShirt.toImages();
  $('#frontShirt-result-preview').val(frontShirtResult.preview);
  $('#frontShirt-result-details').val(JSON.stringify(frontShirtResult.details));
  $('#frontShirt-result-previewTemplate').val(frontShirtResult.previewTemplate);
  $('#backShirt-result-preview').val(backShirtResult.preview);
  $('#backShirt-result-details').val(JSON.stringify(backShirtResult.details));
  $('#backShirt-result-previewTemplate').val(backShirtResult.previewTemplate);
});

/*--------------------*/
// on document ready
$(document).ready(function(){
  $('.shirt-options a:first-child').click(); // set active shirt
  $('.change-background a:first-child img').click(); // set shirt bg
});

// on text edit
$('#canvas-frontShirt').on('text:edit', function(evt){
  let color = evt.detail.properties.fill.replace('#','');
  $('#add-text-value').val(evt.detail.text);
  $('#add-text-family').val(evt.detail.fontFamily);
  $('#add-text-color').val(color);
  $('#add-text').modal('show').attr('data-id',evt.detail.id).attr('data-properties', evt.detail.properties).attr('data-editing','true');
});

$('#canvas-backShirt').on('text:edit', function(evt){
  let color = evt.detail.properties.fill.replace('#','');
  $('#add-text-value').val(evt.detail.text);
  $('#add-text-family').val(evt.detail.fontFamily);
  $('#add-text-color').val(color);
  $('#add-text').modal('show').attr('data-id',evt.detail.id).attr('data-properties', evt.detail.properties).attr('data-editing','true');
});
/*--------------------*/
// define canvas
let defaultConfig = {
  debug : true,
  export : {
    format : 'jpeg',
    quality : 1
  },
  dpi : 200, // dot per inch
  canvas : {
    width : 27.6, // canvas width in cm
    height : 43.8, // canvas height in cm
    offsetX : 16.5, // canvas offset X in cm
    offsetY : 7.8 // canvas offset Y in cm
  },
  wrapper : {
    width : 60, // wrapper width in cm
    height : 60 // wrapper height in cm
  },
  maxScale : 1
};

var frontShirt = new canvasModel('canvas-frontShirt', defaultConfig); // create canvas instance for front shirt
var backShirt = new canvasModel('canvas-backShirt', defaultConfig); // create canvas instace for back shirt

var activeShirt = 'frontShirt';

/*--------------------*/
// functions
// trigger change background on click
$('.change-background a img').click(function(){
  let bg = $(this).attr('src');
  let targetObject = $(this).closest('.bg-options').attr('data-object');
  if(targetObject == 'frontShirt'){
    frontShirt.setBackground(bg);
  }else{
    backShirt.setBackground(bg);
  }
});

// change shirt
$('.change-shirt a').click(function(){
  let targetObject = $(this).attr('data-object');
  // change active shirt
  activeShirt = targetObject;
  // show and hide option
  $('.change-background .bg-options').addClass('hidden');
  $('.change-background .bg-options.' + targetObject).removeClass('hidden');
  // show and hide canvas
  $('.canvas-wrapper').addClass('hidden');
  $('.canvas-wrapper#canvas-'+targetObject).removeClass('hidden');
});

// upload custom image
$('.upload-image').click(function(evt){
  evt.preventDefault();
  $('#custom-upload-image').click();
});
$('input#custom-upload-image').change(function(){
  let FR = new FileReader();
  FR.onload = function(e){
    if(activeShirt == 'frontShirt'){
      frontShirt.addItem( 'image', e.target.result, 'upload' );
    }else{
      backShirt.addItem( 'image', e.target.result, 'upload' );
    }
    $('input#custom-upload-image').val('');
  }
  FR.readAsDataURL( this.files[0] );
});

// add clipart
$('.btn-add-clipart').click(function(){
  let selectedClipart = $('#add-clipart form input:checked').val();
  if(activeShirt == 'frontShirt'){
    frontShirt.addItem( 'image', selectedClipart , 'clipart' );
  }else{
    backShirt.addItem( 'image', selectedClipart , 'clipart' );
  }
  $('#add-clipart').modal('hide');
  $('#add-clipart form input:checked').prop('checked', false);
});

// add text
var updatePreview = function( properties ){
  let text = $('#add-text-value').val();
  let color = '#' + $('#add-text-color').val();
  let fontFamily = $('#add-text-family').val();
  $('#add-text-preview').text( text );
  $('#add-text-preview').css({
    color : color,
    fontFamily : fontFamily
  });
  $('#add-text-preview').attr('data-value', text);
  if(properties){
    $('#add-text-preview').css(properties);
  }
}
$('#add-text-value').on('keyup',function(){
  if( $(this).val().length == 0 ){
    updatePreview( 'masukan text' );
  }else{
    $('#add-text-preview').attr('data-value', $(this).val());
    updatePreview();
  }
});
$('#add-text-color').change(function(){
  updatePreview( { color : '#' + $(this).val() } );
});
$('#add-text-family').change(function(){
  updatePreview( { fontFamily : $(this).val() } );
});
$('.btn-add-text').click(function(){
  let textValue = $('#add-text-preview').attr('data-value');
  let properties = {
    fontFamily : $('#add-text-family').val(),
    color : ('#' + $('#add-text-color').val())
  };
  if( $('#add-text').attr('data-editing') == 'true' ){
    let textId = $('#add-text').attr('data-id');
    if(activeShirt == 'frontShirt'){
      frontShirt.editText(textId, textValue, properties);
    }else{
      backShirt.editText(textId, textValue, properties);
    }
  }else{
    if(activeShirt == 'frontShirt'){
      frontShirt.addItem( 'text', textValue, properties );
    }else{
      backShirt.addItem( 'text', textValue, properties );
    }
  }
  $('#add-text').attr('data-editing','false').modal('hide');
  $('#add-text-value').val('');
  $('#add-text-color').val('ffffff');
  $('#add-text-family').val('Arial');
  updatePreview();
});
$('.btn-add-text').click(function(){
  $('#add-text').attr('data-editing', 'false');
});

// delete currently selected object
$(document).keyup(function(e) {
  if(e.keyCode == 46){
    if(activeShirt == 'frontShirt'){
      frontShirt.deleteSelected();
    }else{
      backShirt.deleteSelected();
    }
  }
});
$('.delete-selected').click(function(evt){
  evt.preventDefault();
  if(activeShirt == 'frontShirt'){
    frontShirt.deleteSelected();
  }else{
    backShirt.deleteSelected();
  }
});

// export images
$('#export-shirt').click(function(){
  frontShirt.toImages(function(returnData){
    console.log(returnData);
    $('#frontShirt-result-preview').val(returnData.preview);
    $('#frontShirt-result-details').val(JSON.stringify(returnData.details));
    $('#frontShirt-result-previewTemplate').val(returnData.previewTemplate);
  });
  backShirt.toImages(function(returnData){
    console.log(returnData);
    $('#backShirt-result-preview').val(returnData.preview);
    $('#backShirt-result-details').val(JSON.stringify(returnData.details));
    $('#backShirt-result-previewTemplate').val(returnData.previewTemplate);
  });
});

/*--------------------*/
// on document ready
$(document).ready(function(){
  $('.shirt-options a:first-child').click(); // set active shirt
  $('.change-background a:first-child img').click(); // set shirt bg
});

// on text edit
$('#canvas-frontShirt').on('text:edit', function(evt){
  let color = evt.detail.properties.fill.replace('#','');
  $('#add-text-value').val(evt.detail.text);
  $('#add-text-family').val(evt.detail.fontFamily);
  $('#add-text-color').val(color);
  $('#add-text').modal('show').attr('data-id',evt.detail.id).attr('data-properties', evt.detail.properties).attr('data-editing','true');
  updatePreview();
});

$('#canvas-backShirt').on('text:edit', function(evt){
  let color = evt.detail.properties.fill.replace('#','');
  $('#add-text-value').val(evt.detail.text);
  $('#add-text-family').val(evt.detail.fontFamily);
  $('#add-text-color').val(color);
  $('#add-text').modal('show').attr('data-id',evt.detail.id).attr('data-properties', evt.detail.properties).attr('data-editing','true');
  updatePreview();
});
/* Custom Canvas Model */
var canvasCustomItem = function(selectorId){

  /* -------------------- */
  // variables
  var thisCanvas = this;
  var element = document.getElementById(selectorId);
  var elementSize = [0,0]; // element size [width, height] in pixel
  var elementBackground;
  var elementCanvasArea;
  var elementCanvas;
  var canvas;
  var backgroundImage;
  var backgroundSize = [600, 600]; // background size [width, height] in pixel
  var canvasOffset = [27.5, 13]; // canvas offset position [x,y] in percent
  var canvasSize = [46, 73]; // canvas size [width, height] in percent
  var dpi = {
    display : 72,
    print : 300
  }
  var canvasZoomRatio = 0.25;

  var canvasItems = [];
  var canvasItemsCount = 0;

  var activeObject;

  let timer;
  let prevSelection = [];

  let tempCanvasWrapper;

  /* -------------------- */
  /* canvas functions */
  // add item to canvas
  this.addItem = function(itemType, item, properties){
    if(itemType == 'text' || itemType == 'image'){
      // set createdItem
      let createdItem = {};
      createdItem.type = itemType;
      createdItem.id = canvasItemsCount;
      canvasItemsCount++;
      // if type == image
      if(itemType == 'image'){
        createdItem.source = properties;
        if (properties == 'clipart'){
          createdItem.url = item;
        }
        properties = null;
        // export image from URL to base64
        if(item.match(/(?:.jpg|.gif|.png|.jpeg)/gi).length){
          let addedImage = new Image();
          addedImage.crossOrigin = 'Anonymous';
          addedImage.onload = function(){
            let newCanvas = document.createElement('CANVAS');
            newCanvas.class = 'hidden';
            let ctx = newCanvas.getContext('2d');
            newCanvas.width = addedImage.width;
            newCanvas.height = addedImage.height;
            ctx.drawImage(this,0,0);
            item = newCanvas.toDataURL();
            newCanvas.remove();
            createdItem.value = item;
            addImageToCanvas();
          };
          addedImage.src = item;
        }else{
          createdItem.value = item;
          addImageToCanvas();
        }

        var addImageToCanvas = function(){
          fabric.Image.fromURL(item, function(img){
            img.id = createdItem.id;
            img.type = itemType;
            img.source = createdItem.source;
            img.crossOrigin = "Anonymous";
            img.set({ scaleX : 0.25, scaleY : 0.25 });
            canvas.add(img);
            img.center();
            img.setCoords();
            canvas.renderAll();
          });
        }

      }
      // if type == text
      if(itemType == 'text'){
        if(typeof(properties) == 'undefined'){
          return 'please specify "text" properties';
        }
        properties.textAlign = 'center';
        properties.stroke = properties.color;
        properties.fill = properties.color;
        delete properties.color;
        let text = new fabric.Text(item, properties);
        text.id = createdItem.id;
        text.type = 'text';
        text.properties = properties;
        canvas.add(text);
        text.center();
        text.setCoords();
        canvas.renderAll();
      }
      createdItem.value = item;
      createdItem.properties = properties;
      canvasItems.push(createdItem);
    }else{
      return 'please specify item type "text" or "image"';
    }

  }// addItem()

  // change canvas background
  this.canvasBackground = function(bgUrl){
    if(bgUrl){
      elementBackground.style.backgroundImage = 'url(' + bgUrl + ')';
      backgroundImage = bgUrl;
    }else{
      return bgUrl;
    }
  };// canvasBackground()

  // export canvas to image
  this.toImages = function(){
    let returnData = {};
    let canvasObjects = canvas.getObjects();
    // transform text to image
    for(let i = 0; i < canvasObjects.length; i++){
      if(canvasObjects[i].type == 'text'){
        let currentObject = canvasObjects[i];
        let tempCanvas = document.createElement('canvas');
        tempCanvas.setAttribute('id',('temp-canvas-' + currentObject.id));
        tempCanvasWrapper.appendChild(tempCanvas);
        // create new fabric
        let tempFabric = new fabric.Canvas(('temp-canvas-'+currentObject.id));
        tempFabric.setWidth(Math.ceil(currentObject.getWidth() * 4));
        tempFabric.setHeight(Math.ceil(currentObject.getHeight() * 4));
        let text = new fabric.Text(currentObject.text, currentObject.properties);
        text.setScaleX(currentObject.getScaleX() * 4);
        text.setScaleY(currentObject.getScaleY() * 4);
        tempFabric.add(text);
        let dataUrl = tempFabric.toDataURL();
        for(let x = 0; x < canvasItems.length; x++){
          if(canvasItems[x].id == currentObject.id ){
            canvasItems[x].text = currentObject.text;
            canvasItems[x].value = dataUrl;
            tempCanvasWrapper.removeChild(tempCanvasWrapper.childNodes[0]);
          }
        }
      }
    }
    // assign value to returnData
    returnData.preview = canvas.toDataURL();
    returnData.details = canvasItems;
    returnData.previewTemplate = '';
    // create image preview with background image
    var createPreviewImage = function(){
      let tempCanvas = document.createElement('canvas');
      tempCanvas.setAttribute('id',('temp-preview'));
      tempCanvasWrapper.appendChild(tempCanvas);
      let tempFabric = new fabric.Canvas('temp-preview');
      tempFabric.setWidth(backgroundSize[0]);
      tempFabric.setHeight(backgroundSize[1]);
      let tempBackgroundImage = new Image;
      tempBackgroundImage.crossOrigin = 'Anonymous';
      tempBackgroundImage.src = backgroundImage;
      let tempBackgroundImageObject = new fabric.Image(tempBackgroundImage);
      tempFabric.add(tempBackgroundImageObject);
      // fabric.Image.fromURL(backgroundImage, function(img){
      //   img.setTop(0);
      //   img.setLeft(0);
      //   img.scaleToWidth( backgroundSize[0] );
      //   tempFabric.add(img);
      //   tempFabric.renderAll();
      // }, { crossOrigin : 'Anonymous' });
      fabric.Image.fromURL(returnData.preview, function(img){
        img.setLeft( backgroundSize[0] * (canvasOffset[0] / 100 ) );
        img.setTop( backgroundSize[1] * (canvasOffset[1] / 100 ) );
        tempFabric.add(img);
        tempFabric.renderAll();
        returnData.previewTemplate = tempFabric.toDataURL();
        tempCanvasWrapper.removeChild(tempCanvasWrapper.childNodes[0]);
        // window.open(returnData.previewTemplate,'popUpWindow','height=600,width=600,left=0,top=0,,scrollbars=yes,menubar=no');
      }, { crossOrigin : 'Anonymous' });
    };
    createPreviewImage();
    return returnData;
  }// toImage()

  // delete currently selected object
  this.deleteSelected = function(){
    let selectedObject = canvas.getActiveObject();
    if(selectedObject){
      // remove item from array
      for(let i = 0; i < canvasItems.length; i++){
        if(canvasItems[i].id == selectedObject.id){
          canvasItems.splice(i, 1);
        }
      }
      // remote item from canvas
      canvas.remove(selectedObject);
    }
  }// deleteSelected()

  // get objects
  this.getObjects = function(){
    return canvas.getObjects();
  }// getObjects()

  // edit text
  this.editText = function(id, text, properties){
    let objects = canvas.getObjects();
    for(let i = 0; i < objects.length; i++){
      if(objects[i].id == id){
        if(objects[i].type == 'text'){
          objects[i].setText(text);
          objects[i].setFontFamily(properties.fontFamily);
          objects[i].setFill(properties.color);
          objects[i].setStroke(properties.color);
          objects[i].properties.fontFamily = properties.fontFamily;
          objects[i].properties.fill = properties.color;
          objects[i].properties.stroke = properties.color;
          canvas.renderAll();
        }
      }
    }
  }// editText()

  /* -------------------- */
  /* sizing */
  // change element size
  this.elementSize = function(canvasWidth, canvasHeight){
    if(canvasWidth && canvasHeight){
      elementSize = [canvasWidth, canvasHeight];
      element.style.width = elementSize[0] + 'px';
      element.style.height = elementSize[1] + 'px';
    }else{
      return elementSize;
    }
  };
  // change background size
  this.backgroundSize = function(bgWidth, bgHeight){
    if(bgWidth && bgHeight){
      backgroundSize = [bgWidth, bgHeight];
      elementBackground.style.backgroundSize = backgroundSize[0] + 'px ' + backgroundSize[1] + 'px';
    }else{
      return backgroundSize;
    }
  };
  // change canvas size
  this.canvasSize = function(canvasWidth, canvasHeight){
    if(canvasWidth && canvasHeight){
      canvasSize = [canvasWidth, canvasHeight];
      elementCanvasArea.style.width = ((backgroundSize[0] * canvasSize[0]) / 100 ) + 'px';
      elementCanvasArea.style.height = ((backgroundSize[1] * canvasSize[1]) / 100 ) + 'px';
    }else{
      return canvasSize;
    }
  };
  // change canvas offset
  this.canvasOffset = function(offsetX, offsetY){
    if(offsetX && offsetY){
      canvasOffset = [offsetX, offsetY];
      elementCanvasArea.style.left = ((backgroundSize[0] * canvasOffset[0]) / 100 ) + 'px';
      elementCanvasArea.style.top = ((backgroundSize[1] * canvasOffset[1]) / 100 ) + 'px';
    }else{
      return canvasOffset;
    }
  };

  /* -------------------- */
  /* element functions */
  // add and remove canvas border
  this.addBorder = function(){
    elementCanvas.style.border = '1px dashed blue';
    elementCanvas.style.padding = '';
  }
  this.removeBorder = function(){
    elementCanvas.style.border = '';
    elementCanvas.style.padding = '1px';
  }

  /* -------------------- */
  // init canvas
  initCanvas = function(){
    // set data init
    element.setAttribute('data-init', 'true');
    // create element
    let createdBackground = document.createElement('div');
    createdBackground.setAttribute('class', 'canvas-background');
    let createdCanvasArea = document.createElement('div');
    createdCanvasArea.setAttribute('class', 'canvas-area');
    let createdCanvas = document.createElement('canvas');
    createdCanvas.setAttribute('id',('canvas-'+selectorId));
    createdCanvasArea.appendChild(createdCanvas);
    element.appendChild(createdBackground);
    element.appendChild(createdCanvasArea);
    elementBackground = createdBackground;
    elementCanvasArea = createdCanvasArea;
    elementCanvas = createdCanvas;
    // set default styling
    element.style.position = 'relative';
    element.style.display = 'block';
    element.style.width = '100%';
    elementBackground.style.display = 'block';
    elementBackground.style.content = '';
    elementBackground.style.width = '100%';
    elementBackground.style.paddingTop = '100%';
    elementBackground.style.backgroundRepeat = 'no-repeat';
    elementCanvasArea.style.position = 'absolute';
    elementCanvasArea.style.display = 'block';
    elementCanvasArea.style.zIndex = '1';
    elementCanvas.style.width = '100%';
    elementCanvas.style.height = '100%';
    elementCanvas.style.padding = '1px';
    // init sizing
    thisCanvas.elementSize(element.offsetWidth, element.offsetHeight);
    thisCanvas.backgroundSize(element.offsetWidth, element.offsetHeight);
    thisCanvas.canvasSize(canvasSize[0], canvasSize[1]);
    thisCanvas.canvasOffset(canvasOffset[0], canvasOffset[1]);
    // init canvas
    canvas = new fabric.Canvas(('canvas-'+selectorId));
    canvas.setWidth(((canvasSize[0] * elementSize[0]) / 100) - 2 );
    canvas.setHeight(((canvasSize[1] * elementSize[1]) / 100) - 2 );
    canvas.preserveObjectStacking = true;
    // add temp canvas wrapper for base64 convert
    tempCanvasWrapper = document.createElement('div');
    tempCanvasWrapper.setAttribute('class','hidden');
    tempCanvasWrapper.setAttribute('id','temp-wrapper-'+selectorId);
    document.body.appendChild(tempCanvasWrapper);
    // console log
    console.log('Canvas initialized : ' + selectorId);
    console.log(thisCanvas);
  }

  /* -------------------- */
  // on init
  if(element.getAttribute('data-init') != 'true'){
    initCanvas();
  }

  /* -------------------- */
  /* functions */
  // reorder objects
  var reorderObjects = function(){
    let canvasObjects = canvas.getObjects();
    canvasObjects = canvasObjects.sort(function(a,b) {
      return a.id - b.id;
    });
    canvas.renderAll();
  }

  /* -------------------- */
  /* events */
  canvas.on('mouse:down', function(evt){
    thisCanvas.addBorder();
    reorderObjects();
    // double click
    if (evt.target) {
      prevSelection.push(evt.target);
      if(prevSelection.length > 2){
        prevSelection.splice(0, 1)
      }
      // start the timer
      let date = new Date();
      let timeNow = date.getTime();
      if(timeNow - timer < 500) {
        // if same object is double clicked
        if(evt.target.id == prevSelection[0].id){
          // check if object type is text
          if(evt.target.type == "text"){
            var event = new CustomEvent("text:edit", {
              detail : evt.target,
              bubble : false,
              cancelable : true
            });
            element.dispatchEvent(event);
          }
        }
      }
      timer = timeNow;
    }
  })
  // on mouse up
  canvas.on('mouse:up', function(evt){
    thisCanvas.removeBorder();
  })
  canvas.on('object:added', function(evt){
    canvas.discardActiveObject();
    reorderObjects();
  });
  canvas.on('object:selected', function(evt){
    activeObject = canvas.getActiveObject();
    reorderObjects();
  });
  canvas.on('object:scaling', function(evt){
    // prevent image to be over stretched
    if(evt.target.type == 'image'){
      if(evt.target.scaleX > 0.25){
        evt.target.scaleX = 0.25
      }
      if(evt.target.scaleY > 0.25){
        evt.target.scaleY = 0.25
      }
    }
  });

};// canvasModel

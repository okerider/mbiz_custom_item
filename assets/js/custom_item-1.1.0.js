// canvas custom item
var canvasModel = function(selector_id, canvasOptions){

  /* -------------------- */
  /* variables */
  var debug = false;
  var thisCanvas = this;
  var cmToInch = (1/2.54);
  var prefix = 'customItem-';
  var instancePrefix = ('customItem-' + selector_id + '-');
  // config
  this.config = {
    init : false,
    export : {
      format : 'jpeg',
      quality : 1
    },
    dpi : 300,
    dpcm : 118,
    scale : 0,
    width : 27.6, // canvas width in cm
    height : 43.8, // canvas height in cm
    offsetX : 16.5, // canvas offset X in cm
    offsetY : 7.8, // canvas offset Y in cm
    elementWidth : 0,
    elementHeight : 0,
    elementOffsetX : 0,
    elementOffsetY : 0,
    background : '',
    maxScale : 1 // max scale factor of an object
  };
  // canvas objects
  this.objects = function(callback){
    if(callback){
      callback(canvas.getObjects());
    }else{
      return canvas.getObjects();
    }
  };
  // wrapper options
  this.wrapper = {
    width : 60, // wrapper width in cm
    height : 60, // wrapper height in cm
    elementWidth : 0,
    elementHeight : 0
  };
  // helper
  this.helper = {
    timer : null,
    latestSelection : [],
    activeObject : null,
    objectCount : 0
  };
  // elements
  var canvas, 
    canvasBackground,
    canvasArea, 
    canvasElement,
    canvasWrapper;
  // temp elements
  var tempElement,
    tempCanvas,
    tempCtx;

  /* -------------------- */
  /* canvas functions */
  // ----------
  // configure options
  if(canvasOptions.debug){
    debug = true;
  }else{
    debug = false;
  }
  this.config.export = canvasOptions.export || this.config.export;
  this.wrapper.width = canvasOptions.wrapper.width || this.wrapper.width;
  this.wrapper.height = canvasOptions.wrapper.height || this.wrapper.height;
  this.config.width = canvasOptions.canvas.width || this.config.width;
  this.config.height = canvasOptions.canvas.height || this.config.height;
  this.config.offsetX = canvasOptions.canvas.offsetX || this.config.offsetX;
  this.config.offsetY = canvasOptions.canvas.offsetY || this.config.offsetY;
  this.config.dpi = canvasOptions.dpi || this.config.dpi;
  this.config.dpcm = canvasOptions.dpcm || ( this.config.dpi * cmToInch );
  this.config.maxScale = canvasOptions.maxScale || this.config.maxScale;
  // ----------
  // init canvas
  var initCanvas = function(){
    // canvas options

    if(debug){
      console.log('custom_item for ' + selector_id + ' is running on debug mode');
    }
    // setup canvas elements
    // set canvas wrapper
    canvasWrapper = document.getElementById(selector_id);
    canvasWrapper.setAttribute('style', 'position:relative; display:block; overflow:hidden;');
    canvasWrapper.className += (' ' + prefix + 'wrapper');
    // set canvas config
    thisCanvas.config.init = true;
    thisCanvas.config.scale = (canvasWrapper.offsetWidth / (thisCanvas.wrapper.width * thisCanvas.config.dpcm));
    // create element for canvas background
    canvasBackground = new Image;
    canvasBackground.setAttribute('class', (prefix + 'background'));
    canvasBackground.setAttribute('style', 'position:absolute; display:block; content:""; width:100%; height:100%; background-repeat:no-repeat; background-size:100% 100%; z-index:0; top:0; left:0;' ); 
    canvasWrapper.appendChild(canvasBackground);
    // create area for canvas
    canvasArea = document.createElement('div');
    canvasArea.setAttribute('class', (prefix + 'canvas'));
    canvasArea.setAttribute('style', 'position:absolute; display:block; z-index:1; top:0; left:0; padding:1px; box-sizing:content-box;');
    canvasWrapper.appendChild(canvasArea);
    // create canvas
    canvasElement = document.createElement('canvas');
    canvasElement.setAttribute('id', (prefix + selector_id));
    canvasElement.setAttribute('style', 'width:100%; height:100%; padding:1px;');
    canvasArea.appendChild(canvasElement);
    // assign fabricJS to canvas
    canvas = new fabric.Canvas((prefix + selector_id));
    canvas.preserveObjectStacking = true;
    // create temp canvas
    tempCanvas = document.createElement('canvas');
    tempCtx = tempCanvas.getContext('2d');
    tempElement = document.createElement('div');
    tempElement.setAttribute('style', 'display:none;');
    tempElement.setAttribute('class', prefix + 'temp-canvas');
    tempElement.setAttribute('id', (instancePrefix + 'temp'));
    tempElement.appendChild(tempCanvas);
    document.body.appendChild(tempElement);
    // setup canvas sizing, wrapper sizing
    thisCanvas.setWrapperWidth(thisCanvas.wrapper.width);
    thisCanvas.setWrapperHeight(thisCanvas.wrapper.height);
    thisCanvas.setCanvasWidth(thisCanvas.config.width);
    thisCanvas.setCanvasHeight(thisCanvas.config.height);
    thisCanvas.setOffsetX(thisCanvas.config.offsetX);
    thisCanvas.setOffsetY(thisCanvas.config.offsetY);
    if(debug){
      console.log('custom item initialized for : ' + selector_id);
      console.log(thisCanvas);
    }
  };// initCanvas()
  // ----------
  // selection
  var setSelection = function(selection){
    if(selection){
      thisCanvas.helper.activeObject = selection;
    }else{
      thisCanvas.helper.activeObject = null;
    }
  };// setSelection()
  // ----------
  // canvas border
  var canvasBorder = function(state){
    if(state){
      canvasArea.style.border = '1px dashed blue'
      canvasArea.style.padding = '';
    }else{
      canvasArea.style.border = '';
      canvasArea.style.padding = '1px';
    }
  }
  // ----------
  // image url to base64
  var urlToDataURL = function(image_url, callback){
    let tempImage = new Image;
    tempImage.crossOrigin = 'Anonymous';
    tempImage.onload = function(){
      let imageWidth = tempImage.width;
      let imageHeight = tempImage.height;
      let returnImage = '';
      tempCanvas.width = imageWidth;
      tempCanvas.height = imageHeight;
      tempCtx.clearRect(0, 0, imageWidth, imageHeight);
      tempCtx.drawImage(tempImage, 0, 0);
      returnImage = tempCanvas.toDataURL('image/png');
      // reset canvas
      tempCtx.clearRect(0, 0, imageWidth, imageHeight);
      tempCanvas.width = 0;
      tempCanvas.height = 0;
      if(debug){
        console.log('image converted to base64 : ' + image_url);
        console.log(returnImage);
      }
      if(callback){
        callback(returnImage);
      }else{
        return returnImage;
      }
    }
    tempImage.src = image_url;
  }// urlToDataURL()
  // ----------
  // get overview image
  // TO DO : Add options for exporting image size
  var getPreviewDesign = function(callback){
    if(debug){
      console.log('getting preview image');
    }
    // return image as base64 when loaded
    let tempImg = new Image;
    tempImg.onload = function(){
      if(callback){
        callback(tempImg);
      }else{
        return tempImg;
      }
    };
    tempImg.src = canvas.toDataURL({ format : 'png' });
  }// getPreviewDesign()
  // ----------
  // get preview with template
  // TO DO : Add options for exporting image size
  var getPreviewTemplate = function(img, callback){
    if(debug){
      console.log('getting preview image with template');
    }
    tempCanvas.width = thisCanvas.wrapper.elementWidth;
    tempCanvas.height = thisCanvas.wrapper.elementHeight;
    tempCtx.drawImage(canvasBackground, 0, 0, tempCanvas.width, tempCanvas.height);
    tempCtx.drawImage(img, thisCanvas.config.elementOffsetX, thisCanvas.config.elementOffsetY, thisCanvas.config.elementWidth, thisCanvas.config.elementHeight);
    // return image as base64 when loaded
    let tempImg = new Image;
    tempImg.onload = function(){
      if(callback){
        callback(tempImg);
      }else{
        return tempImg;
      }
    };
    tempImg.src = tempCanvas.toDataURL('image/jpeg');
  }// getPreviewTemplate()
  // ----------
  // get canvas original object
  var getObjectsOriginal = function(callback){
    if(debug){
      console.log('getting objects original');
    }
    let tempObj = [];
    thisCanvas.objects(function(canvasObjects){
      for(let obj of canvasObjects){
        tempObj.push(obj.original);
      }
      callback(tempObj);
    });
  }// getObjectsOriginal()

  /* -------------------- */
  /* canvas methods */
  // ---------- 
  // set canvas background
  this.setBackground = function(backgroundUrl){
    if(backgroundUrl){
      thisCanvas.wrapper.background = backgroundUrl;
      canvasBackground.src = backgroundUrl;
    }
  };// this.setBackground()

  // ----------
  // canvas sizing
  this.setCanvasWidth = function(width){
    if(width){
      thisCanvas.config.width = width;
      thisCanvas.config.elementWidth = thisCanvas.config.width * thisCanvas.config.dpcm * thisCanvas.config.scale;
      canvasArea.style.width = thisCanvas.config.elementWidth + 'px';
      canvas.setWidth(thisCanvas.config.elementWidth);
    }
  };// this.setCanvasWidth()
  this.setCanvasHeight = function(height){
    if(height){
      thisCanvas.config.height = height;
      thisCanvas.config.elementHeight = thisCanvas.config.height * thisCanvas.config.dpcm * thisCanvas.config.scale;
      canvasArea.style.height = thisCanvas.config.elementHeight + 'px';
      canvas.setHeight(thisCanvas.config.elementHeight);
    }
  };// this.setCanvasHeight()
  this.setWrapperWidth = function(width){
    if(width){
      thisCanvas.wrapper.width = width;
      thisCanvas.wrapper.elementWidth = canvasWrapper.offsetWidth;
      canvasWrapper.style.width = '100%';
    }
  };// this.setWrapperWidth()
  this.setWrapperHeight = function(height){
    if(height){
      thisCanvas.wrapper.height = height;
      thisCanvas.wrapper.elementHeight = height * thisCanvas.config.dpcm * thisCanvas.config.scale;
      canvasWrapper.style.paddingTop = thisCanvas.wrapper.elementHeight + 'px';
    }
  };// this.setWrapperHeight()
  this.setOffsetX = function(coor){
    if(coor){
      thisCanvas.config.offsetX = coor;
      thisCanvas.config.elementOffsetX = coor * thisCanvas.config.dpcm * thisCanvas.config.scale;
      canvasArea.style.left = thisCanvas.config.elementOffsetX + 'px';
    }
  }// this.setOffsetX()
  this.setOffsetY = function(coor){
    if(coor){
      thisCanvas.config.offsetY = coor;
      thisCanvas.config.elementOffsetY = coor * thisCanvas.config.dpcm * thisCanvas.config.scale;
      canvasArea.style.top = thisCanvas.config.elementOffsetY + 'px';
    }
  }// this.setOffsetY()

  // ----------
  // add item to canvas
  this.addItem = function(itemType, item, properties){
    if(itemType == 'text' || itemType == 'image'){
      let originalItem = {
        id : thisCanvas.helper.objectCount,
        type : itemType,
        value : item,
        properties : properties
      };
      thisCanvas.helper.objectCount++;
      if(itemType == 'image'){
        // add image to canvas
        var addImageToCanvas = function(imageUrl){
          fabric.Image.fromURL(imageUrl, function(img){
            img.original = originalItem;
            img.id = originalItem.id;
            img.type = originalItem.type;
            img.source = originalItem.source;
            // rescale image if image bigger than canvas
            if( (img.width * thisCanvas.config.scale) > thisCanvas.config.elementWidth ){
              let newScale = (thisCanvas.config.elementWidth / img.width) - 0.01;
              img.set({ scaleX : newScale, scaleY : newScale, lockScalingFlip : true });
            }else{
              img.set({ scaleX : thisCanvas.config.scale , scaleY : thisCanvas.config.scale , lockScalingFlip : true });
            }
            if(debug){
              console.log('Adding item to canvas : ');
              console.log(img);
            }
            canvas.add(img);
            img.center();
            img.setCoords();
            canvas.renderAll();
          });
        }// addImageToCanvas()
        // check if item is URL
        if(item.match(/\.(jpg|png|gif|jpeg)/gi) != null){
          originalItem.source = 'clipart';
          urlToDataURL(item, function(imageUrl){
            addImageToCanvas(imageUrl);
          });
        }else{
          originalItem.source = 'upload';
          addImageToCanvas(item);
        }
      }else if(itemType == 'text'){
        if(typeof(properties) == 'undefined'){
          return 'please specify "text" properties';
        }
        // additional properties
        properties.stroke = properties.color;
        properties.fill = properties.color;
        // delete color properties
        delete properties.color;
        // create new text for fabric
        let text = new fabric.Text(item, properties);
        text.original = originalItem;
        text.id = originalItem.id;
        text.type = 'text';
        text.properties = properties;
        canvas.add(text);
        text.center();
        text.setCoords();
        canvas.renderAll();
      }
    }else{
      return 'please specify item type "text" or "image"';
    }
  };// this.addItem()

  // ----------
  // export canvas to images
  this.toImages = function(callback){
    let returnData = {
      preview : null,
      details : null,
      previewTemplate : null
    };

    // get preview design
    getPreviewDesign(function(previewDesign){
      returnData.preview = previewDesign.src;
      // document.body.appendChild(previewDesign);
      if(debug){
        window.open(returnData.preview);
      }
      getObjectsOriginal(function(objectsOriginal){
        returnData.details  = objectsOriginal;
        getPreviewTemplate(previewDesign, function(previewTemplate){
          returnData.previewTemplate = previewTemplate.src;
          if(debug){
            window.open(returnData.previewTemplate);
          }
          if(callback){
            callback(returnData);
          }else{
            return returnData;
          }
          // callback(returnData);
        });
      });
    });

  };// this.toImages()

  // ----------
  // delete selected object
  this.deleteSelected = function(){
    canvas.remove(thisCanvas.helper.activeObject);
    setSelection();
  };// this.deleteSelected()

  // ----------
  // edit text
  this.editText = function(id, text, properties){
    thisCanvas.objects(function(canvasObjects){
      for(let obj of canvasObjects){
        if(obj.id == id){
          if(obj.type == 'text'){
            obj.setText(text);
            obj.setFontFamily(properties.fontFamily);
            obj.setFill(properties.color);
            obj.setStroke(properties.color);
            obj.properties.fontFamily = properties.fontFamily;
            obj.properties.fill = properties.color;
            obj.properties.stroke = properties.color;
            canvas.renderAll();
          }
        }
      }
    });
  };// this.editText()

  /* -------------------- */
  /* init canvas */
  if(thisCanvas.config.init == false){
    initCanvas();
  };

  /* -------------------- */
  /* canvas events */
  // ----------
  // on mouse down
  canvas.on('mouse:down', function(evt){
    canvasBorder(true);
    // double click
    if (evt.target) {
      thisCanvas.helper.latestSelection.push(evt.target);
      if(thisCanvas.helper.latestSelection.length > 2){
        thisCanvas.helper.latestSelection.splice(0, 1)
      }
      // start the timer
      let date = new Date();
      let timeNow = date.getTime();
      if(timeNow - thisCanvas.helper.timer < 500) {
        // if same object is double clicked
        if(evt.target.id == thisCanvas.helper.latestSelection[0].id){
          // check if object type is text
          if(evt.target.type == "text"){
            var event = new CustomEvent("text:edit", {
              detail : evt.target,
              bubble : false,
              cancelable : true
            });
            canvasWrapper.dispatchEvent(event);
          }
        }
      }
      thisCanvas.helper.timer = timeNow;
    }// double click
  });

  // ----------
  // on mouse up
  canvas.on('mouse:up', function(evt){
    canvasBorder(false);
  });
  // ----------
  // on object added
  canvas.on('object:added', function(evt){
    console.log(evt);
    canvas.setActiveObject(evt.target);
  });
  // ----------
  // on object selected
  canvas.on('object:selected', function(evt){
    setSelection(evt.target);
  });
  // ----------
  // on selection cleard
  canvas.on('selection:cleard', function(evt){
    setSelection(evt.target);
  });
  // ----------
  // on object scaling
  canvas.on('object:scaling', function(evt){
    // keep size
    if(evt.target.type == 'image'){
      if(evt.target.scaleX > (thisCanvas.config.scale * thisCanvas.config.maxScale)){
        evt.target.scaleX = (thisCanvas.config.scale * thisCanvas.config.maxScale)
      }
      if(evt.target.scaleY > (thisCanvas.config.scale * thisCanvas.config.maxScale)){
        evt.target.scaleY = (thisCanvas.config.scale * thisCanvas.config.maxScale)
      }
    }
  });

}; // canvasModel
